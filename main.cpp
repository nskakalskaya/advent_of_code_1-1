#include <iostream>
#include <string>
#include <sstream>
#include <fstream>



int main()
{
    std::string inputFilename{"/home/qxz0abk/advent_of_code/advent_of_code_1/input_1.txt"};
    /*
    std::cout << "Enter file name: " << std::endl;
    std::cin >> inputFilename;
     */
    std::ifstream inputStream;
    std::string line;
    inputStream.exceptions(std::ifstream::badbit);
    try {
        inputStream.open(inputFilename);
        unsigned int totalSum = 0;
        while (std::getline(inputStream, line)) {
            std::istringstream iss(line);
            int mass;
            if (!(iss >> mass)) {
                break;
            }
            const auto countSum = [](int mass) -> unsigned int { return (mass / 3) - 2; };
            totalSum += countSum(mass);
        }
        std::cout << totalSum << std::endl;
    }
    catch (const std::ifstream::failure &e) {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    inputStream.close();
    return 0;
}
